# Copier Python PDM CLI Template

This template is a starting point for creating a new Python CLI project. 

It includes a number of best practices and tools to help you get started.

It assumes use of `pdm` for managing the package development and makes heavy use of this package.

The template is intended to be passed to pdm init (as per the pdm docs). I have found it useful
to set up an alias command to do this. 

